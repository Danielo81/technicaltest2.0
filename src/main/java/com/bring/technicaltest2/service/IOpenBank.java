package com.bring.technicaltest2.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bring.technicaltest2.dto.BackBaseDTO;
import com.bring.technicaltest2.dto.BackBaseTotalAmountDTO;

@Service
public interface IOpenBank {
	
	public List<BackBaseDTO> getTransactions(String bank, String account);
	
	public List<BackBaseDTO> getTransactionByType(String bank, String account, String type);
	
	public BackBaseTotalAmountDTO getTotalAmountByTxType(String bank, String account, String type);
	
}

