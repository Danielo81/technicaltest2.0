package com.bring.technicaltest2.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bring.technicaltest2.dto.BackBaseDTO;
import com.bring.technicaltest2.dto.BackBaseTotalAmountDTO;
import com.bring.technicaltest2.dto.TransactionOpenBank;
import com.bring.technicaltest2.dto.TransactionOpenBankList;
import com.bring.technicaltest2.service.IOpenBank;
import com.sun.webkit.BackForwardList;

@Service
@Component
public class IOpenBankImpl implements IOpenBank {

	@Override
	public List<BackBaseDTO> getTransactions(String bank, String account) {
		final String uri = "https://apisandbox.openbankproject.com/obp/v1.2.1/banks/" + bank + "/accounts/" + account
				+ "/public/transactions";
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
		mappingJacksonHttpMessageConverter
				.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
		restTemplate.getMessageConverters().add(mappingJacksonHttpMessageConverter);
		TransactionOpenBankList txOpenBankList = restTemplate.getForObject(uri, TransactionOpenBankList.class);
		List<TransactionOpenBank> txOpenBank = txOpenBankList.getTransactions();
		List<BackBaseDTO> backList = new ArrayList<>();
		for (TransactionOpenBank objTx : txOpenBank) {
			BackBaseDTO objB = new BackBaseDTO();
			objB.setId(objTx.getId());
			objB.setAccountId(objTx.getThis_account().getId());
			objB.setCounterpartyAccount(objTx.getOther_account().getNumber());
			objB.setCounterPartyLogoPath(objTx.getOther_account().getMetadata().getImage_URL());
			objB.setInstructedAmount(objTx.getDetails().getValue().getAmount());
			objB.setInstructedCurrency(objTx.getDetails().getValue().getCurrency());
			objB.setTransactionAmount(objTx.getDetails().getValue().getAmount());
			objB.setTransactionCurrency(objTx.getDetails().getValue().getCurrency());
			objB.setTransactionType(objTx.getDetails().getType());
			objB.setDescription(objTx.getDetails().getDescription());
			backList.add(objB);
		}
		return backList;
	}

	@Override
	public List<BackBaseDTO> getTransactionByType(String bank, String account, String type) {
		List<BackBaseDTO> backList = getTransactions(bank, account);
		return backList.stream().filter(b -> b.getTransactionType().equals(type)).collect(Collectors.toList());
	}

	@Override
	public BackBaseTotalAmountDTO getTotalAmountByTxType(String bank, String account, String type) {
		BackBaseTotalAmountDTO totalAmountObj = new BackBaseTotalAmountDTO();
		List<BackBaseDTO> backList = getTransactionByType(bank, account, type);
		double totalAmount = backList.stream().mapToDouble(b -> b.getTransactionAmount()).sum();
		if (backList.size() < 1) {
			totalAmountObj.setTotalAmount(-1);
			totalAmountObj.setTransactionType("transactions not found ");
		} else {
			totalAmountObj.setTotalAmount(totalAmount);
			totalAmountObj.setTransactionType(type);
		}
		return totalAmountObj;
	}

}
