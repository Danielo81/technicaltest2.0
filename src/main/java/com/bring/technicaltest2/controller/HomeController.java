package com.bring.technicaltest2.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bring.technicaltest2.dto.BackBaseDTO;
import com.bring.technicaltest2.dto.BackBaseTotalAmountDTO;
import com.bring.technicaltest2.service.IOpenBank;

@Controller
public class HomeController {
	
	@Autowired
	private IOpenBank iOpenBank;
	
	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) throws IOException{
		return new ModelAndView("home");
	}
	
	
	@RequestMapping(value="/transactions/banks/{name}/account/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> getTransactions(@PathVariable("name") String bank, @PathVariable("id") String account){
		List<BackBaseDTO> response = new ArrayList<>();
		response = iOpenBank.getTransactions(bank, account);
		if(response.size()<1)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not found");
		return ResponseEntity.ok(response);
		
	}
	
	@RequestMapping(value="/transactions/banks/{name}/account/{id}/type/{idType}", method = RequestMethod.GET)
	public ResponseEntity<Object> getTransactionsByType(@PathVariable("name") String bank, @PathVariable("id") String account, @PathVariable("idType") String type){
		List<BackBaseDTO> response = new ArrayList<>();
		response =  iOpenBank.getTransactionByType(bank, account, type);
		if(response.size()<1)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Type account not found");
		return ResponseEntity.ok(response);
	}
	
	@RequestMapping(value="/transactions/amount/banks/{name}/account/{id}/type/{idType}", method = RequestMethod.GET)
	public ResponseEntity<Object> getAmountForTxType(@PathVariable("name") String bank, @PathVariable("id") String account, @PathVariable("idType") String type){
		BackBaseTotalAmountDTO response = new BackBaseTotalAmountDTO();
		response =  iOpenBank.getTotalAmountByTxType(bank, account, type);
		if(response.getTotalAmount()==-1)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Transactions not found");
		return ResponseEntity.ok(response);
	}
	
}
