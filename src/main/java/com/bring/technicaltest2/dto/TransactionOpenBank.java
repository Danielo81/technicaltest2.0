package com.bring.technicaltest2.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionOpenBank implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private ThisAccount this_account;
	private OtherAccount other_account;
	private Details details;
	private Metadata metadata;
	private List<String> transaction_attributes;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ThisAccount getThis_account() {
		return this_account;
	}
	public void setThis_account(ThisAccount this_account) {
		this.this_account = this_account;
	}
	public OtherAccount getOther_account() {
		return other_account;
	}
	public void setOther_account(OtherAccount other_account) {
		this.other_account = other_account;
	}
	public Details getDetails() {
		return details;
	}
	public void setDetails(Details details) {
		this.details = details;
	}
	public Metadata getMetadata() {
		return metadata;
	}
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}
	public List<String> getTransaction_attributes() {
		return transaction_attributes;
	}
	public void setTransaction_attributes(List<String> transaction_attributes) {
		this.transaction_attributes = transaction_attributes;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
