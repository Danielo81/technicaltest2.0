package com.bring.technicaltest2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata {
	
	private String public_alias;
	private String private_alias;
	private String more_info;
	private String URL;
	private String image_URL;
	private String open_corporates_url;
	private String corporate_location;
	private String physical_location;
	private String narrative;
	private List<String> comments;
	private List<String> tags;
	private List<String> images;
	private String where;
	
	
	
	public String getPublic_alias() {
		return public_alias;
	}
	public void setPublic_alias(String public_alias) {
		this.public_alias = public_alias;
	}
	public String getPrivate_alias() {
		return private_alias;
	}
	public void setPrivate_alias(String private_alias) {
		this.private_alias = private_alias;
	}
	public String getMore_info() {
		return more_info;
	}
	public void setMore_info(String more_info) {
		this.more_info = more_info;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getImage_URL() {
		return image_URL;
	}
	public void setImage_URL(String image_URL) {
		this.image_URL = image_URL;
	}
	public String getOpen_corporates_url() {
		return open_corporates_url;
	}
	public void setOpen_corporates_url(String open_corporates_url) {
		this.open_corporates_url = open_corporates_url;
	}
	public String getCorporate_location() {
		return corporate_location;
	}
	public void setCorporate_location(String corporate_location) {
		this.corporate_location = corporate_location;
	}
	public String getPhysical_location() {
		return physical_location;
	}
	public void setPhysical_location(String physical_location) {
		this.physical_location = physical_location;
	}
	public String getNarrative() {
		return narrative;
	}
	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}
	public List<String> getComments() {
		return comments;
	}
	public void setComments(List<String> comments) {
		this.comments = comments;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public List<String> getImages() {
		return images;
	}
	public void setImages(List<String> images) {
		this.images = images;
	}
	public String getWhere() {
		return where;
	}
	public void setWhere(String where) {
		this.where = where;
	}
	
	
	

}
