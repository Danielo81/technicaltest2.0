package com.bring.technicaltest2.dto;

public class BackBaseTotalAmountDTO {
	

	private double totalAmount;
	private String transactionType;
	
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	
	

}
