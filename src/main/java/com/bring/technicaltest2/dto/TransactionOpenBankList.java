package com.bring.technicaltest2.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionOpenBankList {
	
	private List<TransactionOpenBank> transactions;
	
		
	public TransactionOpenBankList() {
		transactions = new ArrayList<TransactionOpenBank>();
	}
	

	public List<TransactionOpenBank> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionOpenBank> transactions) {
		this.transactions = transactions;
	}
	
	
	
	

}
